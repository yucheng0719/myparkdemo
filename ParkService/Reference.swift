//
//  Reference.swift
//  ParkService
//
//  Created by JasonLee on 2019/11/5.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

/*
參考資料網站：
Realm:
        https://realm.io/cn/docs/swift/latest/#bundling-a-realm

 2. Medium:

        https://medium.com/@riccione83/easy-use-of-realm-in-swift-444f41a5742d
    https://medium.com/@mikru168/ios-%E4%BD%BF%E7%94%A8realm%E4%BE%86%E5%AF%A6%E7%8F%BE%E5%A2%9E%E4%BF%AE%E5%88%AA%E6%9F%A5%E7%9A%84%E5%8A%9F%E8%83%BD-3a4b5522f7a6
 
3.其他:
      https://www.jianshu.com/p/5f4d870a3e43
 


 Moya
 1.Medium:
 https://medium.com/@lihsinplayer/swift-library%E5%AD%B8%E7%BF%92%E7%B4%80%E9%8C%84-moya-b5b17bad99c6
 
 2. 其他
    https://www.hangge.com/blog/cache/detail_1797.html
    https://tomoya92.github.io/2018/06/14/swift-moya/
    https://juejin.im/post/5b56854451882562814961a5
 
 3. 延伸 Codable使用與:
    https://www.appcoda.com.tw/swift4-changes/

 4. Moya + RxSwift +  ObjectMapper
    https://www.itread01.com/content/1545666130.html
 
 
 MVVM
 
    https://www.codementor.io/koromiko/mvvm-app-cl1wvw2sh
    https://www.appcoda.com.tw/mvvm-vs-mvc/
    https://www.appcoda.com.tw/tableview-mvvm/
 
 SwiftLint
    https://juejin.im/post/5a3869b95188252103345d26
 
 
 JSONDecoder
  https://medium.com/%E5%BD%BC%E5%BE%97%E6%BD%98%E7%9A%84-swift-ios-app-%E9%96%8B%E7%99%BC%E5%95%8F%E9%A1%8C%E8%A7%A3%E7%AD%94%E9%9B%86/%E5%88%A9%E7%94%A8-swift-4-%E7%9A%84-jsondecoder-%E5%92%8C-codable-%E8%A7%A3%E6%9E%90-json-%E5%92%8C%E7%94%9F%E6%88%90%E8%87%AA%E8%A8%82%E5%9E%8B%E5%88%A5%E8%B3%87%E6%96%99-ee793622629e
 
    https://medium.com/@jerrywang0420/codable-json-%E6%95%99%E5%AD%B8-swift-4-46aff2182bfe
 
      https://zhuanlan.zhihu.com/p/50043306
 
      http://swiftcafe.io/post/codable
 
 
 
 Sign in with Apple
 
     https://medium.com/@avitsadok/ios-13-sign-in-with-apple-tutorial-b71bb3f68de
     https://medium.com/better-programming/adopting-sign-in-with-apple-f75988ece191
 
 */
