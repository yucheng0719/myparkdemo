//
//  Park.swift
//  ParkService
//
//  Created by yucheng Li on 2019/10/29.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import RealmSwift

class Park: Object {

    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var parkId: Int = 0                  // 停車場的編號
    @objc dynamic var type: Int = 0                    // 1:剩餘車位數 2:靜態停車場資料

    @objc dynamic var name: String = ""               // 停車場名稱
    @objc dynamic var area: String = ""               // 行政區
    @objc dynamic var serviceTime: String = ""        // 開放時間
    @objc dynamic var telephone: String = ""          // 停車場電話
    @objc dynamic var addtress: String = ""           // 停車場地址
    @objc dynamic var summary: String = ""            // 停車場概況
    @objc dynamic var payEx: String = ""              // 停車場收費資訊

    @objc dynamic var latitude: Double    = 0.0        // X座標值
    @objc dynamic var longitude: Double   = 0.0        // Y座標值
    @objc dynamic var carGrid: Int      = 0            // 汽車總車位數
    @objc dynamic var motorGrid: Int    = 0            // 機車總格位數
    @objc dynamic var bikeGrid: Int     = 0            // 腳踏車總車架數
    @objc dynamic var lastTime: Date    = Date()

    override class func primaryKey() -> String? {
        return "id"
    }
}

class User: Object {

    @objc dynamic var uid: String = UUID().uuidString
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var age: Int = 0
    @objc dynamic var gender: String = ""
    @objc dynamic var lastTime: Date = Date()

    @objc dynamic var fullName: String = ""

    override class func primaryKey() -> String? {
        return "uid"
    }
}
