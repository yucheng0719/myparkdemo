//
//  RealmStoreData.swift
//  ParkService
//
//  Created by yucheng Li on 2019/10/29.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import Foundation
import RealmSwift

let queueParkBG = "Queue_Park_BG"
let queueNormlBG = "Queue_Norml_BG"

/* ==== 版本更新 ==== */

func setUserDBMigration() {
    
    Realm.Configuration.defaultConfiguration = Realm.Configuration(
        schemaVersion: 3,
        migrationBlock: { (migration, oldSchemaVersion) in
            
            if oldSchemaVersion < 1 {
                migration.enumerateObjects(ofType: User.className()) { (oldObj, newObj) in
                    
                    // Schema版本升級與控管 (新增)
                    let firstName = oldObj!["firstName"] as! String
                    let lastName = oldObj!["lastName"] as! String
                    newObj!["fullName"] = "\(firstName) \(lastName)"
                    
                    // Schema版本升級與控管 (修改)
                    //                        migration.renameProperty(onType: User.className(), from: "fullName", to: "userName")
                }
            }
    })
    
}

/* ====  新增 ==== */

func saveObject<T: Object>(_ object: T, handle:(() -> Void)? = nil) {
    
    do {
        let realm = try Realm()
        try realm.write {
            realm.add(object)
        }
        print("save object at thread = \(Thread.current)")
        
        handle?()
    } catch let error as NSError {
        print("error:saveObject  reason = \(error.localizedDescription)")
    }
}

func saveObjects<T: Object>(_ objects: [T], handle:(() -> Void)? = nil) {
    
    guard !objects.isEmpty else {
        handle?()
        return
    }
    
    DispatchQueue(label: queueNormlBG, qos: .background).async {
        do {
            let realm = try Realm()
            realm.beginWrite()
            realm.add(objects)
            try realm.commitWrite()
            
            print("save objects at thread = \(Thread.current)")
                 
            DispatchQueue.main.async {
                handle?()
            }
        } catch let error as NSError {
            print("error:saveObjects  reason = \(error.localizedDescription)")
        }
    }
}

/* ==== 檢索 ==== */
func getUserObjects() -> Results<User>? {
    do {
        let realm = try Realm()
        let userDB = realm.objects(User.self)
        return userDB
        
    } catch let error as NSError {
        print("error:getUserObjects  reason = \(error.localizedDescription)")
        return nil
    }
}

func getParkObjects(area: String? = nil) -> Results<Park>? {
    do {
        let realm = try Realm()
        if let area = area {
            let parkDB = realm.objects(Park.self).filter("area = %@", area)
            return parkDB
        } else {
            let parkDB = realm.objects(Park.self)
            return parkDB
        }
        
    } catch let error as NSError {
        print("error:getParkObjects  reason = \(error.localizedDescription)")
        return nil
    }
}

/* ==== 更新 ==== */

func updateUserObject(user: User) {
    
    DispatchQueue(label: queueParkBG, qos: .background).async {
        autoreleasepool {
            do {
                
                let realm = try Realm()
                if realm.object(ofType: User.self, forPrimaryKey: user.uid) != nil {
                    try realm.write {
                        realm.add(user, update: .modified)
                    }
                    print("update object at thread = \(Thread.current)")
                    
                } else {
                    print("No found object!!")
                }
            } catch let error as NSError {
                print("error:updateUserObject  reason = \(error.localizedDescription)")
            }
        }
    }
}

func updateParkDB(_ list: [Park], handle: (() -> Void)? = nil) {
    
    DispatchQueue(label: queueParkBG, qos: .background).async {
        autoreleasepool {
            do {
                let realm = try Realm()
               
                // DB 不存在新的資料, 舊的資料從DB中移除
                let parkIds = list.map { $0.parkId }
                let delObjs = realm.objects(Park.self).filter("NOT parkId IN %@", parkIds)
                try realm.write {
                    realm.delete(delObjs)
                }
            
                // 新的資料不在DB 中, DB 加入新資料
                let existObjs = realm.objects(Park.self).filter("parkId IN %@", parkIds)
                
                let realmIds = Array(existObjs).map { $0.parkId }
                
                let tempList = list.filter {
                    return !realmIds.contains($0.parkId)
                }
                
                try realm.write {
                    realm.add(tempList)
                }
                
                // DB 資料更新
                realm.beginWrite()
                for park in list {
                    
                    if let object = realm.objects(Park.self).filter("parkId = %d", park.parkId).last {
                        updateParkDataByDB(old: object, new: park)
                        realm.add(object, update: .modified)
                    }
                }
                try realm.commitWrite()
             
                handle?()
            } catch let error as NSError {
                print("error:updateParkObjects  reason = \(error.localizedDescription)")
                
            }
        }
    }
}

/* ==== 刪除 ==== */
func deleteFromDB(object: Object) {
    do {
        let realm = try Realm()
        try realm.write {
            realm.delete(object)
        }
    } catch let error as NSError {
        print("error:deleteFromDB  reason = \(error.localizedDescription)")
    }
}

func deleteAllFromDB() {
    do {
        let realm = try Realm()
        try realm.write {
            realm.deleteAll()
        }
    } catch let error as NSError {
        print("error:deleteAllFromDB  reason = \(error.localizedDescription)")
    }
}

/* ============ */

func getDBFilePath() {
    do {
        let realm = try Realm()
        print("realmDB path = \(realm.configuration.fileURL!)")
    } catch let error as NSError {
        print("error:getDBFilePath  reason = \(error.localizedDescription)")
    }
}

// MARK: 停車場資訊

func convertRecords(records: [Records]?) -> [Park] {
    
    var array = [Park]()
    
    guard let records = records else {
        return array
    }

    for item in records {
        let park = Park()

        if item.ID != "" {
            park.parkId = Int(item.ID)!
        }
        
        if item.TYPE != "" {
            park.type = Int(item.TYPE)!
        }
        
        if item.TW97X != "", item.TW97Y != "" {
           let coordinate = toLatitudeLongitude(x: Double(item.TW97X)!, y: Double(item.TW97Y)!)
            park.latitude = coordinate.0
            park.longitude = coordinate.1
        }
        
        if item.TOTALCAR != "" {
            park.carGrid = Int(item.TOTALCAR)!
        }
        
        if item.TOTALMOTOR != "" {
            park.motorGrid = Int(item.TOTALMOTOR)!
        }
        
        if item.TOTALBIKE != "" {
            park.bikeGrid = Int(item.TOTALBIKE)!
        }
        
        park.name           = item.NAME
        park.area           = item.AREA
        park.serviceTime    = item.SERVICETIME
        park.telephone      = item.TEL
        park.addtress       = item.ADDRESS
        park.summary        = item.SUMMARY
        park.payEx          = item.PAYEX
        
        array.append(park)
    }

    return array
}

func updateParkDataByDB(old oldPark: Park, new newPark: Park) {
    
    oldPark.parkId       = newPark.parkId
    oldPark.type         = newPark.type
    oldPark.name         = newPark.name
    oldPark.area         = newPark.area
    oldPark.serviceTime  = newPark.serviceTime
    oldPark.telephone    = newPark.telephone
    oldPark.addtress     = newPark.addtress
    oldPark.summary      = newPark.summary
    oldPark.payEx        = newPark.payEx
    oldPark.latitude     = newPark.latitude
    oldPark.longitude    = newPark.longitude
    oldPark.carGrid      = newPark.carGrid
    oldPark.motorGrid    = newPark.motorGrid
    oldPark.bikeGrid     = newPark.bikeGrid
}
