//
//  ServerObjects.swift
//  ParkService
//
//  Created by JasonLee on 2019/11/19.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import Foundation

struct GovResp: Decodable {
    
    var success: Bool = false
    var result: GovResult?
    
    enum CodeKeys: CodingKey {
        case success, result
    }
}

struct GovResult: Decodable {
    var resource_id: String = ""
    var limit: Int = 0
    var total: Int = 0
    var fields: [Fields]?
    var records: [Records]?
    
    enum ResultKeys: CodingKey {
        case resource_id, limit, total, records
    }
}

struct Fields: Decodable {
    var type: String
    var id: String
}

struct Records: Decodable {
    var ID: String
    var TYPE: String
    var TW97X: String
    var TW97Y: String
    var TOTALCAR: String
    var TOTALMOTOR: String
    var TOTALBIKE: String
    var NAME: String
    var AREA: String
    var SERVICETIME: String
    var TEL: String
    var ADDRESS: String
    var SUMMARY: String
    var PAYEX: String

}

struct GovUser: Decodable {
    var success: Bool = false
    var returnCode: Int = -999
    
}

// MARK: - CodingKeys
/* =====
    CodingKeys 對於稍複雜一些的數據結構使用
  =====
 */

extension GovResp {

    public init(from decoder: Decoder) throws {

        do {
            let container = try decoder.container(keyedBy: CodeKeys.self)
            success = try container.decode(Bool.self, forKey: .success)
            result = try container.decode(GovResult.self, forKey: .result)

        } catch let error as NSError {
            print("GovResp decoder error = \(error.localizedDescription)")
        }

    }
}

extension GovResult {

    public init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: ResultKeys.self)
            resource_id = try container.decode(String.self, forKey: .resource_id)
            limit = try container.decode(Int.self, forKey: .limit)
            total = try container.decode(Int.self, forKey: .total)
            records = try container.decode([Records].self, forKey: .records)

        } catch let error as NSError {
            print("GovResult decoder error = \(error.localizedDescription)")
        }
    }
}
