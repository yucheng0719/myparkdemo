//
//  NetworkService.swift
//  ParkService
//
//  Created by JasonLee on 2019/11/6.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import Foundation
import Moya

enum GovParkService {
    case parks, createUser(firstName: String, lastName: String)
}

extension GovParkService: TargetType {
    var baseURL: URL {
        return URL(string: "https://data.ntpc.gov.tw/api/v1")!
    }

    var path: String {
        switch self {
        case .parks: return "/rest/datastore/382000000A-000225-002"
        case .createUser: return "假設這是一段使用Post的路徑"
        }
    }

    var method: Moya.Method {
        switch self {
        case .parks:
            return .get
        case .createUser:
            return .post
        }
    }

    var parameter: [String: Any]? {
        switch self {
        case .parks:
            return nil
        case .createUser(let firstName, let lastName):
            return ["firstName": firstName, "lastName": lastName]
        }

    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .parks:
            return .requestPlain
        case .createUser:
            print("param = \(self.parameter!)")

            return .requestParameters(parameters: self.parameter!, encoding: URLEncoding.default)
        }

    }

    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }

}
