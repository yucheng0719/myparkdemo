//
//  ServerAPI.swift
//  ParkService
//
//  Created by yucheng Li on 2019/9/30.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import Foundation
import Moya

protocol ServerAPIProtocol {
    func fetchParks(handler:@escaping (_ response: APIResult) -> Void)
    func createUsers(firstName: String, lastName: String, handler:@escaping (_ response: APIResult) -> Void)
}

class ServerAPI: ServerAPIProtocol {
    
    let provider = MoyaProvider<GovParkService>()

    func fetchParks(handler: @escaping (APIResult) -> Void) {
       
        provider.request(.parks) { (result) in
            switch result {
            case .success(let resp):

                do {
                    let respJson = try JSONDecoder().decode(GovResp.self, from: resp.data)
                    handler(.success(response: respJson))

                } catch let error as NSError {
                   
                    print("error:fetchParks reason = \(error.localizedDescription)")
                    handler(.msgErr(message: error.localizedDescription))
                }
                
            case .failure(let error):
                
                handler(.msgErr(message: error.localizedDescription))
            }
        }
    }
    
    // 模擬使用的Post方法
    func createUsers(firstName: String, lastName: String, handler: @escaping (APIResult) -> Void) {
        
        provider.request(.createUser(firstName: "Lee", lastName: "Jason")) { ( result) in
            switch result {
            case .success(let resp):
                do {
                    let respJson = try JSONDecoder().decode(GovUser.self, from: resp.data)
                    handler(.success(response: respJson))

                } catch let error as NSError {
                    print("error:createUsers reason = \(error.localizedDescription)")
                    handler(.msgErr(message: error.localizedDescription))
                }
            case .failure(let error):
                handler(.msgErr(message: error.localizedDescription))
            }
        }
    }
}
