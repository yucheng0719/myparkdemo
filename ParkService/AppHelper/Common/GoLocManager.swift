//
//  GoLocManager.swift
//  ParkService
//
//  Created by yucheng Li on 2019/10/1.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit
import MapKit

enum LocationResult {
    case success(lat: Double, long: Double)
    case noUpdated
}

class GoLocManager: NSObject {
    static let share = GoLocManager()
    var locationManager = CLLocationManager()

    private var handler: ((LocationResult) -> Void)?
    private var userLocation:(lat: Double, long: Double) = (25.013510, 121.464079) // Banqiao station 
    private var locationServices: Bool = false

    func setLocation() {

        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = kCLLocationAccuracyKilometer
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer

        if CLLocationManager.authorizationStatus() == .denied {
            print("Location services were previously denied.")
            print("Please enable location services for this app in Settings.")
            locationServices = false
            return
        }

        if CLLocationManager.locationServicesEnabled() {
            locationServices = true
            locationManager.startUpdatingLocation()
        }

        print(CLLocationManager.authorizationStatus())
    }

    func startUpdatingLocation(completion:@escaping ((LocationResult) -> Void)) {
        self.handler = completion

        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }

    }

    func stopUpdatingLocation() {
        self.handler = nil

        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }

    func currentLocation() -> (lat: Double, long: Double) {
        return (userLocation.lat, userLocation.long)
    }

    func findDistance(by lat: Double, long: Double) -> Double {

        let userLoc = CLLocation(latitude: userLocation.lat, longitude: userLocation.long)
        let destination =  CLLocation(latitude: lat, longitude: long)
        let distance: CLLocationDistance = destination.distance(from: userLoc)

        return (locationServices) ? distance : -99
    }
}

extension GoLocManager: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else {
            self.handler?(.noUpdated)
            return
        }

        self.userLocation.lat = locValue.latitude
        self.userLocation.long = locValue.longitude
        self.handler?(.success(lat: self.userLocation.lat, long: self.userLocation.long))
    }
}
