//
//  Alert.swift
//  ParkService
//
//  Created by JasonLee on 2019/10/30.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit

class Alert {

    func showAlert(vc viewController: UIViewController,
                   title: String?,
                   message: String?,
                   showCancel: Bool = false,
                   handle: ((Bool) -> Void)? = nil) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "確認", style: .default, handler: { _ in
            handle?(true)
        }))

        if showCancel {
            alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: { _ in
                handle?(false)
            }))
        }
        viewController.present(alert, animated: true, completion: nil)
    }

    func showAlert(vc viewController: UIViewController,
                   title: String?,
                   message: String?,
                   dismissAfter time: Double,
                   handle: ((Bool) -> Void)? = nil) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            handle?(true)
            alert.dismiss(animated: true, completion: nil)
        }

        viewController.present(alert, animated: true, completion: nil)
    }

}
