//
//  MapSnapshot.swift
//  ParkService
//
//  Created by yucheng Li on 2019/10/1.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import MapKit

class MapSnapshot: NSObject {

    var itemW = UIScreen.main.bounds.width
    var itemH = UIScreen.main.bounds.height / 1.5

    func snapshotMap(latitude lat: Double, longitude lng: Double, completion:@escaping() -> Void) {

        let mapRegion = MKCoordinateRegion(center: CLLocationCoordinate2DMake(lat, lng),
                                      span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))

        let options = MKMapSnapshotter.Options()
        options.region = mapRegion
        options.mapType = .standard
        options.scale = 2.0
        options.size = CGSize(width: itemW, height: itemH)
        options.showsBuildings = true
        options.showsPointsOfInterest = true

        let bgQueue = DispatchQueue.global(qos: .background)
        let snapShotter = MKMapSnapshotter(options: options)

        snapShotter.start(with: bgQueue, completionHandler: { (snapshot, error) in

            DispatchQueue.main.async {
                guard let snapshot = snapshot, error == nil else {
                    print("\(error.debugDescription)")
                    return
                }

                let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                let rect = CGRect(x: 0,
                                  y: 0,
                                  width: snapshot.image.size.width,
                                  height: snapshot.image.size.height)

                UIGraphicsBeginImageContextWithOptions(options.size, true, 0)
                snapshot.image.draw(at: .zero)

                let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
                let pinImage = pinView.image

                var point = snapshot.point(for: location)

                if rect.contains(point) {
                    let pinCenterOffset = pinView.centerOffset
                    point.x -= pinView.bounds.size.width / 2
                    point.y -= pinView.bounds.size.height / 2
                    point.x += pinCenterOffset.x
                    point.y += pinCenterOffset.y
                    pinImage?.draw(at: point)
                }

                let image = UIGraphicsGetImageFromCurrentImageContext()

                UIGraphicsEndImageContext()

                self.writeSnapshot(image: image, completion: {
                    completion()
                })
            }
        })

    }

    // MARK: Private Methods
    private func writeSnapshot(image: UIImage?, completion:@escaping() -> Void) {

        guard let image = image else { return }

        let fileName = "snapshot.jpg"
        let fileURL = AppHelper.share.getDocumentsDirectory().appendingPathComponent(fileName)

        if FileManager.default.fileExists(atPath: fileURL.path) {
            do { try FileManager.default.removeItem(at: fileURL) } catch {}
        }

        if let data = image.jpegData(compressionQuality: 1.0) {
            do {
                try data.write(to: fileURL)
                completion()
            } catch {
                print("error saving file:", error)
            }
        }
    }
}
