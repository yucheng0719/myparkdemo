//
//  AppHelper.swift
//  ParkService
//
//  Created by yucheng Li on 2019/10/1.
//  Copyright © 2019 yucheng Li. All rights reserved.
//
import UIKit

class AppHelper: NSObject {
    static let share = AppHelper()

    private var myUserDef: UserDefaults!

    override init() {
        myUserDef = UserDefaults.standard
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    // MARK: - User Default
    func updateUserInfo(value: Any, for key: String) {
        myUserDef.set(value, forKey: key)
        myUserDef.synchronize()
    }

    func removeUserInfo(for key: String) {
        myUserDef.removeObject(forKey: key)
    }

    func getValueViaUserInfo(for key: String) -> Any? {

        if let item = myUserDef.object(forKey: key) {
           return item
        }
        return nil
    }

}
