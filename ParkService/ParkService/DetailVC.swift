//
//  DetailVC.swift
//  ParkService
//
//  Created by yucheng Li on 2019/9/30.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit
import MapKit

class DetailVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!

    let parkList: [ParkDetailEnum] = ParkDetailEnum.allCases
    var cellVM: ParkListCell!

    override func viewDidLoad() {
        super.viewDidLoad()

        indicatorView.startAnimating()
        takeMapSnapShot()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.tableView.reloadData()
    }

    @IBAction func didMapTaaped(_ sender: Any) {
        openMap()
    }

    @IBAction func didBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func takeMapSnapShot() {
        let mapSnapshot = MapSnapshot()
        mapSnapshot.itemW = UIScreen.main.bounds.width
        mapSnapshot.itemH = 300
        mapSnapshot.snapshotMap(latitude: cellVM.latitude, longitude: cellVM.longitude) {
            let fileName = "snapshot.jpg"
            let fileURL = AppHelper.share.getDocumentsDirectory().appendingPathComponent(fileName)

            if FileManager.default.fileExists(atPath: fileURL.path) {
                self.imgView.image = UIImage(contentsOfFile: fileURL.path)!
            }

            self.indicatorView.stopAnimating()
        }
    }

    func openMap() {

        let location = GoLocManager.share.currentLocation()

        let source = MKMapItem(placemark:
            MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: location.lat,
                                                           longitude: location.long)))
        source.name = "您的位置所在處"

        let destination = MKMapItem(placemark:
            MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: cellVM.latitude,
                                                           longitude: cellVM.longitude)))
        destination.name = cellVM.name

        let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        MKMapItem.openMaps(with: [source, destination],
                           launchOptions: options)
    }
}

extension DetailVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parkList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell",
                                                       for: indexPath) as? ParkDetailCell
        else {
            fatalError("Cell not exists in storyboard")
        }

        let item = parkList[indexPath.item]
        cell.title = item.name()

        switch item {
        case .name:     cell.detail = cellVM.name
        case .area:     cell.detail = cellVM.area
        case .service:  cell.detail = cellVM.service
        case .address:  cell.detail = cellVM.address
        }
        return cell
    }

}
