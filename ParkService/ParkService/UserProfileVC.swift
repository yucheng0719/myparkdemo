//
//  UserProfileVC.swift
//  ParkService
//
//  Created by yucheng Li on 2019/10/29.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit
import RealmSwift

class UserProfileVC: UIViewController {

    @IBOutlet weak var deleteItem: UIBarButtonItem!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!

    var status: CRUDEnum!

    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
        method(status: .retrieve)

    }

    func setupTextFields() {
        lastNameTF.text     = user!.lastName
        firstNameTF.text    = user!.firstName
        ageTF.text          = "\(user!.age)"
        genderTF.text       = user!.gender

        print("user uid = \(user!.uid) Date = \(user!.lastTime)")
    }

    func cleanTextFields() {
        lastNameTF.text = ""; firstNameTF.text = ""; ageTF.text = ""; genderTF.text = ""
    }

    @IBAction func didSaveTapped(_ sender: Any) {
        self.view.endEditing(true)

        if self.user != nil {
            updateUserData()
        } else {
            saveUserData()
        }
    }

    @IBAction func didDeleteTapped(_ sender: Any) {
        self.view.endEditing(true)
        
        Alert().showAlert(vc: self, title: "確認刪除資料麼？", message: nil, showCancel: true) { confirm in

            guard confirm else { return }

            self.method(status: .delete, object: self.user!)
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension UserProfileVC {

    func saveUserData() {
        guard checkCondition() else { return }

        let object = User()
        object.lastName = lastNameTF.text!
        object.firstName = firstNameTF.text!
        object.gender = genderTF.text!
        object.age = Int(ageTF.text!)!
        method(status: .create, object: object)
    }

    func updateUserData() {
        guard checkCondition() else { return }
                
        let object = User()
        object.uid = user!.uid
        object.lastName = lastNameTF.text!
        object.firstName = firstNameTF.text!
        object.gender = genderTF.text!
        object.age = Int(ageTF.text!)!

        method(status: .update, object: object)
    }

    func checkCondition() -> Bool {

        guard let lastName = lastNameTF.text, lastName != "" else {
            Alert().showAlert(vc: self, title: "錯誤", message: "姓名空白", dismissAfter: 3.0)
            return false
        }
        guard let firstName = firstNameTF.text, firstName != "" else {
            Alert().showAlert(vc: self, title: "錯誤", message: "名字空白", dismissAfter: 3.0)
            return false
        }
        guard let gender = genderTF.text, gender != "" else {
            Alert().showAlert(vc: self, title: "錯誤", message: "性別空白", dismissAfter: 3.0)
            return false
        }
        guard let age = ageTF.text, let ageInt = Int(age), ageInt > 0 else {
            Alert().showAlert(vc: self, title: "錯誤", message: "年齡不正確", dismissAfter: 3.0)
            return false
        }
        return true
    }

    func method(status: CRUDEnum, object: User? = nil) {

        switch status {
        case .create:

            saveObject(object!) {
                self.user = object!
                self.deleteItem.isEnabled = true
                Alert().showAlert(vc: self, title: "加入成功", message: nil, dismissAfter: 3.0)
            }

        case .retrieve:
            
            if let userDB = getUserObjects(), !userDB.isEmpty {
                user = userDB.last!
                deleteItem.isEnabled = true
                setupTextFields()
            }
        
        case .update:

            updateUserObject(user: object!)

            deleteItem.isEnabled = true
            Alert().showAlert(vc: self, title: "更新成功", message: nil, dismissAfter: 3.0)

        case .delete:

            if let user = object {
                deleteFromDB(object: user)
                self.cleanTextFields()
                self.deleteItem.isEnabled = false
                self.user = nil

            } else {
                Alert().showAlert(vc: self, title: "操作錯誤", message: "刪除")
            }
        }
    }
}

extension UserProfileVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.canResignFirstResponder { textField.resignFirstResponder()
        }
        return true
    }
}
