//
//  MainVC.swift
//  ParkService
//
//  Created by yucheng Li on 2019/9/30.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit
import SocialSignIn

class MainVC: UIViewController {

    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    let googleService = GoogleService()
    let facebookService = FBService()
    let appleService = AppleService()
    
    var googleSignIn: Bool = false {
        didSet {
            let title = !self.googleSignIn ? "Sign In with Google" : "Sign Out with Google"
            googleBtn.setTitle(title, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GoLocManager.share.setLocation()

        setUserDBMigration()

        // Google
        setModels()
        googleService.setDelegate(vc: self)
        googleService.checkSingIn()
        
        //Facebook
        checkFBSignIn()
        
        //Apple
        appleService.createAppleBtn(to: containerView)
        appleService.setDelegate(vc: self)
        appleService.mProtocol = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func didFBSignInPressed(_ sender: Any) {
        
        let permission = FBPermission()
        permission.publicProfile = ["public_profile": ["name"]]
        facebookService.signIn(withPermission:
        [permission.publicProfile, permission.email],
                               viewController: self) { (result) in
            switch result {
            case .success(let user):
                print("user = \(user.name!)")
                print("user rawValue = \(user.rawValue!)")

            case .error(let error):
                print(error)
            
            case .cancel:
                print("user cancel")
                
            case .signout:
                print("user signout")
            }
        }
        
    }
    
    @IBAction func didGoogleSignInPressed(_ sender: Any) {
        googleService.signInOut(signIn: !googleSignIn)
    }
    
    // MARK: Google Methods
    func setModels() {
        googleService.onResult = { result in
            
            switch result {
            case .success(let user):
                self.googleSignIn = true
                print("Sign In")
                print("User Token = \(user.accessToken)")
                print("User Email = \(user.email!)")
                
            case .error(let error):
                self.googleSignIn = false
                print("Error = \(error.localizedDescription)")
                
            case .cancel:
                print("Cancel")
                
            case .signout:
                self.googleSignIn = false
                print("Sign Out")
                
            }
        }
    }
    
    // MARK: FB Methods
    func checkFBSignIn() {
        if facebookService.checkAuth() {
            facebookBtn.setTitle("Sign Out with Facebook", for: .normal)
        } else {
            facebookBtn.setTitle("Sign In with Facebook", for: .normal)
        }
    }
    
    func signInFB() {
          let permission = FBPermission()
          permission.publicProfile = ["public_profile": ["name"]]

          facebookService.signIn(withPermission:
              [permission.publicProfile, permission.email],
                                 viewController: self) { (result) in

              switch result {
              case .success(let user):
                  print("user = \(user.name!)")
                  print("user rawValue = \(user.rawValue!)")

              case .error(let error):
                  print(error)

              case .cancel:
                  print("user cancel")

              case .signout:
                  print("user signout")
              }

              self.checkFBSignIn()
          }
      }

      func signOutFB() {
          facebookService.signOut()
          checkFBSignIn()
      }
}

extension MainVC: AppleServiceProtocol {
    
    func appleSignInAuth(state: AppleAuthEnum) {
        switch state {
        case .authorized:
            print("User is authorized to continue using your app")
            
        case .revoked:
            print("User has revoked access to your app")
            
        case .notFound:
            print("User is not found, meaning that the user never signed in through Apple ID")
            
        case .transferred:
            print("User has transferred access")
            
        case .error:
            print("Receipt error from Apple")
            
        }
    }
    
    func appleSignIn(user: String?, email: String?, userID: String?) {
        print("Sign in with Apple: sign in successful")
        
        if let user = user {
            print("User = \(user)")
        }
        if let email = email {
            print("Email = \(email)")
        }
    }
    
    func onError(error: Error) {
        print("error = \(error.localizedDescription)")
    }
    
}
