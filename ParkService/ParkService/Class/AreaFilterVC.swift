//
//  AreaFilterVC.swift
//  ParkService
//
//  Created by JasonLee on 2019/10/1.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit

protocol AreaFilterProtocol {
    func areaFilterList(list: [String])
}

class AreaFilterVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var areaList: [String] = ["萬里區", "金山區", "板橋區", "汐止區", "深坑區", "石碇區",
                             "瑞芳區", "平溪區", "雙溪區", "貢寮區", "新店區", "坪林區",
                             "烏來區", "永和區", "中和區", "土城區", "三峽區", "樹林區",
                             "鶯歌區", "三重區", "新莊區", "泰山區", "林口區", "蘆洲區",
                             "五股區", "八里區", "淡水區", "三芝區", "石門區"]

    var selectedItem: [String] = [String]()
    var mProtocol: AreaFilterProtocol?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animation()

    }

    @IBAction func didBackgroundTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    func animation() {
        self.tableView.alpha = 0
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.tableView.alpha = 1
        }, completion: nil)
    }

}

extension AreaFilterVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return areaList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let item = areaList[indexPath.item]

        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath)
        cell.textLabel?.text = item
        cell.accessoryType = selectedItem.contains(item) ? .checkmark : .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let item = areaList[indexPath.item]

        if let mIndex = selectedItem.firstIndex(of: item) {
            selectedItem.remove(at: mIndex)
        } else {
            selectedItem.append(item)
        }

        self.tableView.reloadRows(at: [IndexPath(item: indexPath.item, section: 0)], with: .none)

        self.mProtocol?.areaFilterList(list: selectedItem)
    }

}
