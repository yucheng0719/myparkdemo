//
//  ParkDetailCell.swift
//  ParkService
//
//  Created by yucheng Li on 2019/10/1.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit

class ParkDetailCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!

    var title: String? = nil {
        didSet {
            self.titleLabel.text = self.title
        }
    }

    var detail: String? = nil {
        didSet {
            self.detailLabel.text = self.detail
        }
    }

}
