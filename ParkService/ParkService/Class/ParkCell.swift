//
//  ParkCell.swift
//  ParkService
//
//  Created by yucheng Li on 2019/9/30.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit

class ParkCell: UITableViewCell {

    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!

    var area: String? = nil { didSet { self.areaLabel.text = self.area } }
    var service: String? = nil { didSet { self.serviceLabel.text = self.service } }
    var name: String? = nil { didSet { self.titleLabel.text = self.name } }
    var address: String? = nil { didSet { self.addressLabel.text = self.address } }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
