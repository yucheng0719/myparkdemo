//
//  RxSwiftDemoVC.swift
//  ParkService
//
//  Created by YuChengLi on 2019/12/5.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit
import RxSwift

class RxSwiftDemoVC: UIViewController {
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func demoJust() {
        
        let observable = Observable.just("Hello!! RxSwift")
        
        observable.subscribe(onNext: { element in
            
            print("element = \(element)")
        }, onError: { error in
            
            print("onError = \(error.localizedDescription)")
        }).disposed(by: disposeBag)
    }
    
    func demoFrom() {
        
        let observable = Observable<Double>.from([1, 2, 3])
        observable.subscribe(onNext: { element in
            
            print("element = \(element)")
        }).disposed(by: disposeBag)
    }
    
    func demoMap() {
        
        let disposeBag = DisposeBag()
        let observable = Observable.of(1, 2, 3, 4)
        observable.map {
            $0 * 100
            
        }.subscribe(onNext: { element in
            
            print("element = \(element)")
        }).disposed(by: disposeBag)
    }
    
    func demoZip() {
        
        let disposeBag = DisposeBag()
        let first  = PublishSubject<String>()
        let second = PublishSubject<String>()
        
        Observable.zip(first, second) { $0 + " " + $1 + " Group" }
            .subscribe(onNext: {
                
                print($0)
            })
            .disposed(by: disposeBag)
        
        first.onNext("貓貓")
        first.onNext("狗狗")
        first.onNext("羊羊")
        first.onNext("兔兔")
        
        second.onNext("A")
        second.onNext("B")
        second.onNext("C")
        second.onNext("D")
    }
    
    func demoObserveOn() {
        
        Observable<Int>.create { observer in
            observer.onNext(1)
            sleep(1)
            observer.onNext(2)
            return Disposables.create()
        }
        .observeOn(SerialDispatchQueueScheduler(qos: .background))
        .map { value -> Int in
            
            print("\n\n value = \(Thread.current)")
            return value * 2
        }
        .observeOn(MainScheduler.instance)
        .map { value -> Int in
            
            print("value = \(Thread.current)")
            return value * 3
        }
        .observeOn(SerialDispatchQueueScheduler(qos: .background))
        .subscribe(onNext: { element in
            
            print("element = \(element) \(Thread.current)")
        }).disposed(by: disposeBag)
    }
    
    func demoSubscribeOn() {
        
        Observable<Int>.create { observer in
            
            observer.onNext(1)
            sleep(1)
            observer.onNext(2)
            return Disposables.create()
        }
        .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
        .map { value -> Int in
            
            print("\n\n value = \(Thread.current)")
            return value * 2
        }
        .map { value -> Int in
            
            print("value = \(Thread.current)")
            return value * 3
        }
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { element in
            
            print("element = \(element) \(Thread.current)")
        }).disposed(by: disposeBag)
    }
    
    func demoRetry() {
        var count: Int = 1
        
        Observable<String>.create { (observer) -> Disposable in
            observer.onNext("AA")
            observer.onNext("BB")
            observer.onNext("CC")
            
            if count < 2 {
                
                observer.onError(NSError(domain: "Error", code: -1) as Error)
                print("Error encountered")
                count += 1
            }
            
            observer.onNext("DD")
            observer.onNext("EE")
            observer.onNext("FF")
            
            return Disposables.create()
        }
        .retryWhen({ (_) -> Observable<Int> in
            return Observable<Int>.timer(.seconds(3), scheduler: MainScheduler.instance)
        })
            .subscribe(onNext: { element in
                
                print("element = \(element)")
            }).disposed(by: disposeBag)
    }
    
    func demoDelay() {
        
        Observable<Int>.create { observer -> Disposable in
            observer.onNext(1)
            observer.onNext(2)
            
            return Disposables.create()
        }
        .delay(.seconds(5), scheduler: MainScheduler.instance)
        .subscribe(onNext: { element in
            
            print("element = \(element) \(Thread.current)")
        }).disposed(by: disposeBag)
    }
    
    func demoConcat() {
        
        let subject1 = PublishSubject<Int>()
        let subject2 = PublishSubject<Int>()
        
        Observable.concat(subject1, subject2)
            .map {
                $0 * 10
        }
        .subscribe(onNext: { element in
            
            print("element = \(element) \(Thread.current)")
        }).disposed(by: disposeBag)
        
        subject1.onNext(12)
        subject1.onNext(13)
        subject1.onCompleted()
        
        subject2.onNext(99)
        subject2.onNext(100)
        subject2.onCompleted()
        
    }
    
    func demoFlatMap() {
        
        Observable.of(1, 2, 3)
            .map { $0 * 2 }
            .flatMap { elemnet -> Observable<String> in
                
                return Observable.create { observer -> Disposable in
                    observer.onNext("\(elemnet)")
                    return Disposables.create() }
                
        }
        .subscribe(onNext: { element in
            
            print("element = \(element)")
        }, onError: { error in
            
            print("onError = \(error)")
        }).disposed(by: disposeBag)
    }
}

extension RxSwiftDemoVC {
    
    /*
     AsyncSubject
     => 收到完成指令, 發出最後一筆 elment
     */
    
    func demoSubject1() {
        
        let disposeBag = DisposeBag()
        let subject = AsyncSubject<String>()
        
        subject.subscribe {
            print("subscription 1 Event:", $0)
        }.disposed(by: disposeBag)
        
        subject.onNext("狗狗")
        subject.onNext("貓貓")
        subject.onNext("兔兔")
        subject.onCompleted()
    }
    
    /*
        PublishSubject
        => 觀察者訂閱方式取得所有觀察的 elements,
           若 element 發生在訂閱之前的話不會收到任何觀察事件
     */
    
    func demoSubject2() {
        
        let disposeBag = DisposeBag()
        
        let subject = PublishSubject<String>()
        
        subject.onNext("狗狗")
        
        subject.subscribe {
            print("Subscription 1 Event:", $0)
            
        }.disposed(by: disposeBag)
        
        subject.onNext("貓貓")
        subject.onNext("兔兔")
        
        subject.subscribe {
            print("Subscription 2 Event:", $0)
        }.disposed(by: disposeBag)
        
        subject.onNext("AA")
        subject.onNext("BB")
        
    }
    
    /*
        ReplaySubject
        => 觀察者可以接收到所有元素
     */
    
    func demoSubject3() {
        
        let disposeBag = DisposeBag()
        let subject = ReplaySubject<String>.create(bufferSize: 1)
        
        subject.subscribe {
            print("Subscription1 Event:", $0)
            
        }.disposed(by: disposeBag)
        
        subject.onNext("狗狗")
        subject.onNext("貓貓")
        
        subject.subscribe {
            print("Subscription2 Event:", $0)
            
        }.disposed(by: disposeBag)
        
        subject.onNext("AA")
        subject.onNext("BB")
        
    }
    
    /*
        BehaviorSubject
        => 得到最新的結果, element 不存在則發出預設值
    */
    
    func demoSubject4() {
        
        let disposeBag = DisposeBag()
        let subject = BehaviorSubject(value: "Blue")
        
        subject.subscribe {
            print("Subscription1 Event:", $0)
            
        }.disposed(by: disposeBag)
        
        subject.onNext("狗狗")
        subject.onNext("貓貓")
        
        subject.subscribe {
            print("Subscription2 Event:", $0)
        }.disposed(by: disposeBag)
        
        subject.onNext("AA")
        subject.onNext("BB")
        
        subject.subscribe {
            print("Subscription3 Event:", $0)
        }.disposed(by: disposeBag)
        
        subject.onNext("水蜜桃")
        subject.onNext("橘子")
        
    }
}
