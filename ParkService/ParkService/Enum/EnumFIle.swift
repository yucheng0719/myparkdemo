//
//  EnumFIle.swift
//  ParkService
//
//  Created by JasonLee on 2019/10/1.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import Foundation

// MARK: - Flow Control
enum CRUDEnum {
    case create, retrieve, update, delete
}

enum ControlEnum {
    case add, update, process, finished, unknown
}

// MARK: - View
enum ParkDetailEnum: Int, CaseIterable {
    case name = 0, area, service, address

    func name() -> String {
        switch self {
        case .name: return "停車場名稱"
        case .area: return "區域"
        case .service: return "營業時間"
        case .address: return "地址"
        }
    }
}

// MARK: - Code Response
public enum APIResult {
    case success(response: Any)
    case apiErr(error: APIError)
    case msgErr(message: String)
}

public enum APIError: String, Error {
    case noNetwork = "No netimport UIKitwork"
    case serviceOverload = "Service is overloaded"
    case permissionDenied = "You have no permission"
    case paramErr = "parameter error"
    case disconnect = "Disconnect"
    case serverBusy = "Server is busy"
    case unknown = "Unknown error"
}
