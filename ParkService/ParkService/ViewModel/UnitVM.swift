//
//  UnitVM.swift
//  ParkService
//
//  Created by JasonLee on 2019/12/5.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import Foundation

struct ParkListCell {
    
    let name: String
    let address: String
    let area: String
    let service: String
    let latitude: Double
    let longitude: Double
    
}
