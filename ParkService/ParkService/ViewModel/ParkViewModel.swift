//
//  ParkViewModel.swift
//  ParkService
//
//  Created by JasonLee on 2019/11/11.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import Foundation
import RealmSwift
import Moya
import RxSwift

class ParkViewModel {

    let disposeBag = DisposeBag()
    
    var reloadTable:(() -> Void)?
    var updateLoading:(() -> Void)?

    var apiService: ServerAPIProtocol
    var listCells: [ParkListCell] = [ParkListCell]()

    var isLoading: Bool = false {
        didSet {
            self.updateLoading?()
        }
    }

    var numberOfCells: Int {
        return listCells.count
    }

    init() {
        self.apiService = ServerAPI()
    }
    
    func refreshParks() {
        
        apiService.fetchParks { result in
            self.isLoading = false
            
            switch result {
                
            case .success(let object):
                
                guard let obj = object as? GovResp else {
                    self.reloadTable?()
                    return
                }
                
                guard let records = obj.result?.records else {
                    self.reloadTable?()
                    return
                }
                
                var parks = [Park]()
                
                Observable.from(optional: records)
                .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
                .flatMap { records -> Observable<Park> in
                    
                    return Observable.from(convertRecords(records: records))
                }
                .subscribe(onNext: { park in
                    parks.append(park)
           
                }, onError: { error in
                    
                    print("onError = \(error.localizedDescription)")
                }, onCompleted: {

                    updateParkDB(parks) {
                        DispatchQueue.main.async {
                            
                            if let parksDB = getParkObjects() {
                                self.listCells = self.createCells(parks: Array(parksDB))
                            } else {
                                self.listCells = [ParkListCell]()
                            }
                        
                            self.reloadTable?()
                        }
                    }
                    
                    print("onCompleted updateParks Thread = \(Thread.current)")

                }).disposed(by: self.disposeBag)
                                
            case .apiErr(let error):
                
                print("Error = \(error.localizedDescription)")
            case .msgErr(message:  let error):
                
                print("Error = \(error)")
            }
        }
    }

    func getCellViewModel(at indexPath: IndexPath) -> ParkListCell {
           return listCells[indexPath.item]
    }

    // MARK: - Private Methods
    
    private func createCells(parks: [Park]) -> [ParkListCell] {
        
        var cells = [ParkListCell]()
        
        Observable.from(parks)
        .flatMap { park -> Observable<ParkListCell> in
                
            return  Observable.just(ParkListCell(name: park.name, address: park.addtress,
                                                 area: park.area, service: park.serviceTime,
                                             latitude: park.latitude, longitude: park.longitude))
        }
        .subscribe(onNext: { listCell in
            cells.append(listCell)
        }).disposed(by: disposeBag)
        
        return  cells
    }
}
