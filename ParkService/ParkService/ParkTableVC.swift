//
//  ParkTableVC.swift
//  ParkService
//
//  Created by yucheng Li on 2019/9/30.
//  Copyright © 2019 yucheng Li. All rights reserved.
//

import UIKit
import RealmSwift

class ParkTableVC: UIViewController {

    @IBOutlet var noDataView: UIView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!

    lazy var refreshControl = UIRefreshControl()

    let time30mins: Int = 60 * 30

    lazy var viewModel: ParkViewModel = { return ParkViewModel() }()

    var filterItems: [String] = [String]()
    var selectList = [ParkListCell]()

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.attributedTitle = NSAttributedString(string: "下拉更新列表", attributes: nil)
        refreshControl.addTarget(self, action: #selector(pullRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl

        initVM()
    }

    @IBAction func didFilterTaaped(_ sender: Any) {

        let popAreaVC = self.storyboard?.instantiateViewController(withIdentifier: "AreaFilterVC")
            as! AreaFilterVC
        popAreaVC.mProtocol = self
        popAreaVC.selectedItem = filterItems
        popAreaVC.modalPresentationStyle = .overCurrentContext
        popAreaVC.view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        present(popAreaVC, animated: false, completion: nil)
    }

    @IBAction func didBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func initVM() {
        viewModel.updateLoading = {
            let isloading = self.viewModel.isLoading

            if self.viewModel.isLoading {
                self.indicatorView.startAnimating()
            } else {
                self.indicatorView.stopAnimating()
            }
            self.refreshControl.endRefreshing()

            print("Loading ParkAPI = \(isloading)")
        }

        viewModel.reloadTable = {
            self.selectList = self.viewModel.listCells
            self.excuteFilterList()
            self.checkEmpty()
        }

        viewModel.refreshParks()
    }

    func excuteFilterList() {

        if !filterItems.isEmpty {
            let filter = self.viewModel.listCells.filter {
                return self.filterItems.contains($0.area)
            }

            selectList = filter
        } else {

            selectList = self.viewModel.listCells
        }
        self.tableView.reloadData()
    }

    @objc func pullRefresh() {
        viewModel.refreshParks()
    }
}

// MARK: - AreaFilterProtocol

extension ParkTableVC: AreaFilterProtocol {

    func areaFilterList(list: [String]) {
        filterItems = list
        excuteFilterList()
    }
}

// MARK: - TableView
extension ParkTableVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell",
                                                       for: indexPath) as? ParkCell
        else {
            fatalError("Cell not exists in storyboard")
        }

        let item = selectList[indexPath.item]
        cell.name       = item.name
        cell.area       = item.area
        cell.service    = item.service
        cell.address    = item.address
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        detailVC.cellVM = selectList[indexPath.item]

        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    func checkEmpty() {
        tableView.backgroundView = (selectList.isEmpty) ? noDataView : nil
        tableView.reloadData()
    }
}
